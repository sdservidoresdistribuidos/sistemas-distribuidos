package server1;
import server0.ClientFullDuplex.*;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.Post;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.Facebook;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import com.restfb.FacebookClient;
import com.restfb.FacebookClient.AccessToken;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;
import com.restfb.types.Post;
import com.restfb.types.User;
import static java.lang.System.exit;
import java.util.Iterator;
import java.util.List;

public class MultiServerThread extends Thread {
   private Socket socket = null;
    private Object server0;
  

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut="";
		
	     while((lineIn = entrada.readLine()) != null){
          //System.out.println("Received: "+lineIn);
            //escritor.flush();
            //***************************************array con servicios****************************************
           ArrayList<String> serviciosS1 = new ArrayList<String>();
           serviciosS1.add("inv");
           serviciosS1.add("concat");
           serviciosS1.add("may");
           serviciosS1.add("min");
           serviciosS1.add("num2text");
           serviciosS1.add("dec2bin");
           Iterator it = serviciosS1.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
           //***********************************************************************
            String peticion = lineIn;
             String respuesta="";
            String[] parts = peticion.split("#");
            String comando = parts[1]; // va al switch
            String nn = parts[2]; // numero de cadenas
            String cadena=parts[3];//cadena a procesar
         
            String[] argu = null;
            int n;
            n=Integer.parseInt(nn);
            String[] cad=new String[n];
            if(serviciosS1.contains(comando)==true){
            System.out.println("SERVICIO EXISTENTE");
            
              switch(comando){
                case "inv":
                    String CadenaInvertida="";
                    String resp="#";
                        for (int y=0;y<n;y++){
                            cad[y]= parts[y+3];
                            CadenaInvertida="";
                                for (int x=cad[y].length()-1;x>=0;x--){
                                    CadenaInvertida = CadenaInvertida + cad[y].charAt(x);}
                            resp=resp+CadenaInvertida+"#";}
                    respuesta="#r:inv#"+n+resp;
                    System.out.println(respuesta);
                    break;
                case "concat":
                    String CadenaConcat="";
                        for (int y=0;y<n;y++){
                            CadenaConcat= CadenaConcat+parts[y+3];}
                    respuesta="#r:concat#1#"+CadenaConcat+"#";
                    System.out.println(respuesta);
                    break;
                case "may":
                    String may=cadena.toUpperCase();
                    respuesta="#r:may#1#"+may+"#";
                    System.out.println(respuesta);
                    break;
                case "min":
                    String min=cadena.toLowerCase();
                    respuesta="#r:min#1#"+min+"#";
                    System.out.println(respuesta);
                    break;
                case "num2text":
                    String num=parts[3];
                    long parteEntera;
                    parteEntera =Long.parseLong(num);
                    System.out.println(parteEntera);
                    int millon=(int) (parteEntera/1000000);//tantos millones
                    String ll="";

                    if(millon==0){ll=milest((int) parteEntera);}
                    else if(parteEntera==1000000){ll="Un Millon";}
                    else if(millon==1){ll="Un Millon "+milest((int) (parteEntera%1000000));}
                    else if(millon>1){ll=milest(millon)+"Millones "+milest((int) (parteEntera%1000000));}
                    respuesta="#r:num2text#1#"+ll+"#";
                    break;
                case "dec2bin":
                    int numero=Integer.parseInt(cadena);
                    ArrayList<String> binario = new ArrayList<String>();
                    int resto;
                    String binarioString = "";
                    do{
                        resto = numero%2;
                        numero = numero/2;
                        binario.add(0, Integer.toString(resto));
                    }while(numero !=0); //Haremos el bucle hasta que el cociente no se pueda dividir mas
                    binario.add(0, Integer.toString(numero)); //Cogeremos el ultimo cociente

                    for(int i = 1; i < binario.size(); i++){
                    binarioString += binario.get(i);
                    }
                    
                    respuesta="#r:dec2bin#1#"+binarioString+"#";
                    break;
                
            }
            
               
                
            if(lineIn.equals("FIN")){
               ServerMultiClient.NoClients--;
			      break;
			   }else{
               escritor.println("Echo... "+respuesta);
               escritor.flush();
            }
            
            }else{System.out.println("ERROR"); respuesta="#r:ERROR";
            System.out.println("buscando e otro servidor");
            server0.S1 s0 =new server0.S1("localhost",12345);
            String line=s0.Peticion(peticion);
            
            respuesta=line;
            escritor.println(respuesta);
            
            }
            
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
      
      
   private static String triTexto(int n) {
       String result="" ;
        String cent="";
        String dec="";
        String uni="";
        int centenas = n / 100;
        int decenas  = (n % 100) / 10;
        int unidades = (n % 10);
 
        switch (centenas) {
            case 0: cent="";break;//Es un numero menor a 100 
            case 1:
                if (decenas == 0 && unidades == 0) {
                    result="Cien ";
                    return result;
                }
                else cent="Ciento "; 
                break;
            case 2: cent="Doscientos "; break;
            case 3: cent="Trescientos "; break;
            case 4: cent="Cuatrocientos "; break;
            case 5: cent="Quinientos "; break;
            case 6: cent="Seiscientos "; break;
            case 7: cent="Setecientos "; break;
            case 8: cent="Ochocientos " ; break;
            case 9: cent="Novecientos "; break;
        }
       
 
        switch (decenas) {
            case 0: break;
            case 1:
                if (unidades == 0) { dec="Diez "; uni="";return cent+dec+uni;}
                else if (unidades == 1) { dec="Once "; uni=""; return cent+dec+uni;}
                else if (unidades == 2) { dec="Doce "; uni="";return cent+dec+uni;}
                else if (unidades == 3) { dec="Trece ";  uni="";return cent+dec+uni;}
                else if (unidades == 4) { dec="Catorce ";  uni="";return cent+dec+uni;}
                else if (unidades == 5) { dec="Quince "; uni="";return cent+dec+uni;}
                else dec="Dieci";
                break;
            case 2:
                if (unidades == 0) { dec="Veinte "; uni=""; }
                else dec="Veinti";
                break;
            case 3: dec="Treinta "; break;
            case 4: dec="Cuarenta "; break;
            case 5: dec="Cincuenta "; break;
            case 6: dec="Sesenta "; break;
            case 7: dec="Setenta "; break;
            case 8: dec="Ochenta "; break;
            case 9: dec="Noventa "; break;
        }
        
        if (decenas > 2 && unidades > 0){
            dec=dec+"y ";}
 
        switch (unidades) {
            case 0: break;
            case 1: uni="Uno "; break;
            case 2: uni="Dos "; break;
            case 3: uni="Tres "; break;
            case 4: uni="Cuatro "; break;
            case 5: uni="Cinco "; break;
            case 6: uni="Seis "; break;
            case 7: uni="Siete "; break;
            case 8: uni="Ocho "; break;
            case 9: uni="Nueve "; break;
        }
        
        return cent+dec+uni;
    }
  private static String milest(int parteEntera){
        String resultado="";
     int miles=parteEntera/1000;//tantos miles}
    if(miles>0){
        int triUnidades = parteEntera % 1000;//mil+triTexto
        if(parteEntera==0){resultado="Mil";}
        else{
        resultado=triTexto(miles)+"Mil "+triTexto(triUnidades);}
    }
    else{
    resultado=triTexto(parteEntera);
    }
    return resultado;
    }
   public static int hex2decimal(String s) {
             String digits = "0123456789ABCDEF";
             s = s.toUpperCase();
             int val = 0;
             for (int i = 0; i < s.length(); i++) {
                 char c = s.charAt(i);
                 int d = digits.indexOf(c);
                 val = 16*val + d;
                 System.out.println(val);
             }
             return val;
         }
} 
