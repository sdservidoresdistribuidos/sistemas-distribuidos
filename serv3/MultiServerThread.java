package serv3;




import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import static java.lang.System.exit;
import java.util.List;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut="";
	 Diccionario di=new Diccionario();
         di.servciosS3();	
	     while((lineIn = entrada.readLine()) != null){
          //System.out.println("Received: "+lineIn);
            //escritor.flush();
           String peticion = lineIn;
             
            
            String[] parts = peticion.split("#");
            String comando = parts[1]; // va al switch
            String nn = parts[2]; // numero de cadenas
            String cadena=parts[3];//cadena a procesar
            int n;
            n=Integer.parseInt(nn);
            String[] cad=new String[n];
            String respuesta="";
            
            Boolean loc=di.isDefinedLocal(comando);
            if(loc==true){
            System.out.println("SERVICIO EXISTENTE");
           
            
              switch(comando){

                case "concat":
                    String CadenaConcat="";
                        for (int y=0;y<n;y++){
                            CadenaConcat= CadenaConcat+parts[y+3];}
                    respuesta="#r:concat#1#"+CadenaConcat+"#";
                    System.out.println(respuesta);
                    break;
                
                case "min":
                    String min=cadena.toLowerCase();
                    respuesta="#r:min#1#"+min+"#";
                    System.out.println(respuesta);
                    break;
                
                
            }
            
               
                
            if(lineIn.equals("FIN")){
               ServerMultiClient.NoClients--;
			      break;
			   }else{
               escritor.println("Echo... "+respuesta);
               escritor.flush();
            }
            
             }else{System.out.println("ERROR"); respuesta="#r:#ERROR";
             System.out.println("buscando e otro servidor");
            
             serv1.Diccionario dic= new serv1.Diccionario();
             Boolean glo;
             glo= dic.isDefinedGlobal(comando);
            
            
             if(glo==true){
         
            String maquina= dic.maquina(comando);
            int puerto=dic.puerto(comando);
           
          
         //   String [] dat=dic.info();
            serv1.servidorCliente SC =new serv1.servidorCliente(maquina,puerto);
             String line=SC.Peticion(peticion);
            respuesta=line;
            escritor.println(respuesta);
           // System.out.println("Se encontro el servicio en : "+dat[1]+" "+dat[2]);
             }
             else{
                     respuesta="No se puede ejecutar dicha solicitud";
             escritor.println(respuesta);
             }
            
            }
            
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
  
  
} 
